use std::sync::Arc;
use crate::process_starter::ProcessStarter;
use client::di_factory as client_di_factory;

pub struct DiC {
    // Public services
    pub process_starter: Arc<ProcessStarter>,
}

impl DiC {
    pub fn new(
        process_starter: Arc<ProcessStarter>,
    ) -> DiC {

        DiC {
            process_starter,
        }
    }
}

pub fn di_factory() -> DiC {
    let client_di = client_di_factory();

    let process_starter  = Arc::new(ProcessStarter::new(client_di.zeebe_client.clone()));

    DiC::new(
        process_starter,
    )
}
