use std::sync::Arc;
use zeebe::Client;
use log::{info, error};
// use serde_json::json;

pub struct ProcessStarter {
    zeebe_client: Arc<Client>,
}

impl ProcessStarter {
    pub fn new(zeebe_client: Arc<Client>) -> ProcessStarter {
        ProcessStarter {
            zeebe_client,
        }
    }

    pub async fn start_process(&self) -> Result<(), String> {
        let process_definition_id = "Process_0ghvkm3";

        let res = self.zeebe_client
            .create_process_instance()
            .with_bpmn_process_id(process_definition_id)
            .with_latest_version()
            // .with_variables(json!({"myData": 31243}))
            .send()
            .await
        ;

        match res {
            Ok(instance_dto) => {
                info!("Process started{:?}", instance_dto);
                Ok(())
            },
            Err(e) => {
                error!("Some error {:?}", e);
                Err("Some error".to_string())
            }
        }
    }
}
