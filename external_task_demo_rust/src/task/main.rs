extern crate core;

use crate::di::di_factory;

mod di;
mod external_task_handler;
mod external_task_initializer;

#[tokio::main]
async fn main() -> Result<(), String> {
    env_logger::init();

    let di = di_factory();
    di.external_task_initializer.start().await
}
