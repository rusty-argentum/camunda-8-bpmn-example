use std::sync::Arc;
use client::di_factory as client_di_factory;
use crate::external_task_handler::ExternalTaskHandler;
use crate::external_task_initializer::ExternalTaskInitializer;

pub struct DiC {
    // Public services
    pub external_task_initializer: Arc<ExternalTaskInitializer>,
}

impl DiC {
    pub fn new(
        external_task_initializer: Arc<ExternalTaskInitializer>,
    ) -> DiC {

        DiC {
            external_task_initializer,
        }
    }
}

pub fn di_factory() -> DiC {
    let client_di = client_di_factory();

    let external_task_handler = Arc::new(ExternalTaskHandler::new());
    let external_task_initializer= Arc::new(ExternalTaskInitializer::new(client_di.zeebe_client.clone(), external_task_handler));


    DiC::new(
        external_task_initializer,
    )
}
