use futures::future;
use std::sync::Arc;
use serde::{Deserialize, Serialize};
use zeebe::{Client, Data, Job};
use log::{error, info};
use crate::external_task_handler::ExternalTaskHandler;
use thiserror::Error;


pub struct ExternalTaskInitializer {
    zeebe_client: Arc<Client>,
    external_task_handler: Arc<ExternalTaskHandler>,
}

// And app-specific job data
#[derive(Deserialize)]
struct MyJobData {
}

#[derive(Serialize)]
struct MyJobResult {
    ext_task_result: u32,
}

// Given an app-specific error
#[derive(Error, Debug)]
enum MyError {
    #[error("unknown error occurred")]
    Unknown,
}


impl ExternalTaskInitializer {
    pub fn new(zeebe_client: Arc<Client>, external_task_handler: Arc<ExternalTaskHandler>) -> ExternalTaskInitializer {
        ExternalTaskInitializer {
            zeebe_client,
            external_task_handler,
        }
    }

    pub async fn start(&self) -> Result<(), String>  {
        info!("Starting...");
        let handler = self.external_task_handler.clone();
        let res = self
            .zeebe_client
            .job_worker()
            // .with_service("Activity_11uab6s")
            // .with_job_type("Activity_11uab6s")
            // .with_job_type("demo_ext_ext")
            .with_job_type("some_tsk_dfntion")
            .with_auto_handler(move |job: Job, my_job_data: Data<MyJobData>| {
                info!("Auto handler:  {:?}", job);
                handler.handle();
                // future::ok::<(), _>(Result())
                future::ok::<_, MyError>(MyJobResult { ext_task_result: 42 })
            })
            .run()
            .await
        ;

        match res {
            Ok(instance_dto) => {
                info!("Job handler Ok{:?}", instance_dto);
                Ok(())
            },
            Err(e) => {
                error!("Some error {:?}", e);
                Err("Some error".to_string())
            }
        }
    }
}
