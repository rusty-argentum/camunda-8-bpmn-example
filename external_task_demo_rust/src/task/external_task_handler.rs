use core::time;
use std::thread::sleep;
use log::info;

pub struct ExternalTaskHandler {
}

impl ExternalTaskHandler {
    pub fn new() -> ExternalTaskHandler {
        ExternalTaskHandler {}
    }

    pub fn handle(&self) {
        //TODO add param for data
        //TODO return some result
        info!("Data handled.");

        //TODO: implement some activity

        sleep(time::Duration::from_secs(1))
    }
}
