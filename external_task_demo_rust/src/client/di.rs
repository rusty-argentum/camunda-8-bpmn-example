use std::sync::Arc;
use zeebe::{Client, ClientConfig};

pub struct DiC {
    // Public services
    pub zeebe_client: Arc<Client>,
}

impl DiC {
    pub fn new(
        zeebe_client: Arc<Client>,
    ) -> DiC {

        DiC {
            zeebe_client,
        }
    }
}

pub fn di_factory() -> DiC {
    //TODO: get from config
    let endpoints = vec!["http://0.0.0.0:26500".to_string()];
    // let tls = ClientTlsConfig::new();

    let client = Client::from_config(ClientConfig {
        endpoints,
        // tls: Some(tls),
        tls: None,
        auth: None,
    }).unwrap();


    DiC::new(
        Arc::new(client),
    )
}
